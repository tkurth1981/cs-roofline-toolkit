#ifndef KERNEL1_H
#define KERNEL1_H

//necessary for the logicals below
#define FP64  1
#define FP32  2
#define FP16  3

#if ERT_PREC == FP32
#warning using FP32 (float) precision
#define REAL float
#define DISCOUNT static_cast<float>(1e-5)
#elif ERT_PREC == FP16
#warning using FP16 (half) precision
#ifdef ERT_GPU
#include <cuda_fp16.h>
#endif
#define REAL __half
#define DISCOUNT static_cast<__half>(1e-2)
#else
#warning using FP64 (double) precision
#define REAL double
#define DISCOUNT static_cast<double>(1e-8)
#endif

#ifdef ERT_GPU
extern int gpu_blocks;
extern int gpu_threads;
#endif

#define KERNEL1(a,b,c)   ((a) = (b) + (c))
#define KERNEL2(a,b,c)   ((a) = (a)*(b) + (c))


void initialize(uint64_t nsize,
                REAL* __restrict__ array,
                REAL value);

#ifdef ERT_GPU
void gpuKernel(uint64_t nsize,
               uint64_t ntrials,
               REAL* __restrict__ array,
               int* bytes_per_elem,
               int* mem_accesses_per_elem);
#else
void kernel(uint64_t nsize,
            uint64_t ntrials,
            REAL* __restrict__ array,
            int* bytes_per_elem,
            int* mem_accesses_per_elem);
#endif

#endif

